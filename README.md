**Elia Dotor Puente P04-50**  
**Miguel García Insua**


*P04-50 depurador*
==================


Depuración con Eclipse:
----------------------

Primero creamos el proyecto *P04-50-depurador*


Para probar el depurador de eclipse, en primer lugar creamos un punto de ruptura (doble clic barra izquierda):  

![Punto_ruptura](imagenes/puntoruptura.png)

Le damos a *Debug as-->Java Application* y pasamos a la perspectiva debug.

Empieza a ejecutarse el código, entra en el bucle y está esperando a que introduzcamos un número. En el momento que lo introduzcamos va a llegar al punto de ruptura y se parará. En ese momento aparecerá un cuadro de diálogo que diga que va a cambiar la perspectiva a modo depuración:  

![Perspectiva_depuracion](imagenes/perspectivadebug.png)


En esta perspectiva podemos ver todos los programas que tenemos ahora mismo en depuración:  

![Hilos_depuracion](imagenes/hilosdepuracion.png)

Es importante parar cada proceso después de terminar de depurar un programa, ya que si no quedará suspendido pero seguirá en ejecución.


El depurador nos marca en que instrución estamos de color gris.
En nuestro caso se ve que estamos en la instrucción pero que aún no se ha ejecutado.  

![Instruccion](imagenes/instruccion.png)


Una de las ventanas más importantes es la que nos sale a la derecha en la que podemos ver el valor que toman las variables.

![Ventana_variables](imagenes/variables.png)


Además permite cambiar el valor que tiene la variable, de esta forma podemos realizar casos de pruebas:

![Cambio_variables](imagenes/cambiovariables.png)


Con los botones de la perpectiva debug que tenemos arriba a la izquierda, podemos ir ejecutando y viendo el avance de las variables a través de la ejecución de el código.
El rojo(**Terminate**) sirve para pararlo y el verde (**Resume**) para continuar la ejecución aunque se haya parado. 

![Botones_depuracion](imagenes/botones.png)


El primer botón **Step into**, va avanzando intrucción por instrucción.

Vemos que la instrucción del punto de ruptura se ha ejecutado porque nos ha mostrado algo en la consola y pasa a la siguiente instrucción que se encuentra que es el bucle for. Vemos que esta aún no se ha ejecutado porque i sigue valiendo 0.

![Boton_step_into](imagenes/icero.png)

Seguimos avanzando de instrucción en instrucción hasta que i valga 5 por lo tanto ya no entrará en el bucle y saltará a la siguiente instrucción.

![Bucle](imagenes/bucle.png)


Llega a la última instrucción `input.close()`, cierro el scanner y se termino mi programa. 

![Ultima_instrucción](imagenes/ultimainstruccion.png)

Paro la depuración al terminar.

![Paro_depuracion](imagenes/parodepuracion.png)


El segundo botón **Step over** y el tercero **Step return** hacen lo mismo, un avance de instrucción a instrucción pero tienen que ver con los métodos.

Para trabajar con ellos creamos un método dentro de la clase que nos va a devolver un boolean que nos diga si un número es par o no.


Si pulsamos el botón que hemos estado pulsando hasta ahora **Step into**, iremos instrucción a instrucción. Nos pedirá un número, llegará a la instrucción que llama al método `esPar() y entrará en el método a evaluarlo.

Llega a la instrucción:

![Instruccion_a_instruccion](imagenes/instruccion1.png)

Entra en el método:

![Instruccion_a_instruccion](imagenes/instruccion2.png)

Tras finalizar las instrucciones del método vuelve a la instrucción de partida:

![Instruccion_a_instruccion](imagenes/instruccion3.png)



Si en este caso pulsamos el segundo botón **Step over** ya no entramos en el método sino que lo saltamos y continua ejecutandose la siguiente instrucción.

Llega a la instrucción:

![Salto_metodo](imagenes/saltometodo1.png)

Salta el método:

![Salto_metodo](imagenes/saltometodo2.png)

Sigue con la instrucción:

![Salto_metodo](imagenes/saltometodo3.png)



Comprobamos el siguiente botón **Step return**, en esta ocasión introducimos un número, saltamos a la siguiente instrucción y en este caso si entramos en el método, entramos a depurar el código para evaluar si hay algún error. Una vez que encontramos algo queremos salir al punto de la llamada, donde se llamo a este método. Podriamos ir instrucción a instrucción o bien pulsar este botón que sale directamente del método y vuelve al lugar donde se llamo.

Llega a la instrucción:

![Punto_llamada](imagenes/puntollamada1.png)

Entra en el método:

![Punto_llamada](imagenes/puntollamada2.png)

Cuando no quiere seguir comprobando las instrucciones del método, sale directamente y vuelve a la instrucción:

![Punto_llamada](imagenes/puntollamada3.png)

Estos tres botones son las principales herramientas del depurador.

Junto con la ventana de **variables** que hemos visto anteriormente y la ventana de **Breakpoints** que muestra nuestros puntos de ruptura.

![Ventana_breakpoints](imagenes/ventanapuntosruptura.png)  


Depuración con IntelliJ Idea:
-----------------------------

* La primera diferencia la hemos encontrado a la hora de elegir los puntos de ruptura; ya que este IDE tiene 4 tipos diferentes de puntos de ruptura y uno de estos tipos (punto de ruptura por línea) equivale a los puntos de ruptura que habiamos generado con *Eclipse*.  


![Punto_de_ruptura_1](imagenes/punto_de_ruptura_1.png)
  
![Punto_de_ruptura_2](imagenes/punto_de_ruptura_2.png)

* Comenzaremos primero por probar introduciendo un número par y pulsando el botón de **Step Into** y así podremos revisar el funcionamiento del metodo `esPar()`  

![Prueba_par](imagenes/prueba_par_into_1.png)

![Prueba_par](imagenes/prueba_par_into_2.png)

* Ahora probaremos el mismo procedimiento pero introduciendo un número impar.

![Prueba_impar](imagenes/prueba_impar_into_1.png)

![Punto_impar](imagenes/prueba_impar_into_2.png)

![Punto_impar](imagenes/prueba_impar_into_3.png)
 
![Punto_impar](imagenes/prueba_impar_into_4.png)

 
* A continuación vamos a cambiar el valor que ha tomado la variable `i` en este momento de la depuración

![Cambiar_valor](imagenes/cambiar_variable_1.png)

![Cambiar_valor](imagenes/cambiar_variable_2.png)

![Cambiar_valor](imagenes/cambiar_variable_3.png)


* En esta ocasión vamos a depurar el programa pero a la hora de llamar al método `esPar()` vamos a pulsar el botón **Step Out** para no entrar a depurar el propio método sino que solo se evaluará el flujo principal del programa.

![Prueba_step_out](imagenes/prueba_step_out.png)

* Si pulsamos el botón **Step Into** en una línea en la que se llama a algún método de Java podremos ver el código del método y cómo nuestro programa va recorriendo el código de este.

![Ver_ejecucion_scanner](imagenes/metodo_scanner_1.png)

![Ver_ejecucion_scanner](imagenes/metodo_scanner_2.png)

![Ver_ejecucion_scanner](imagenes/metodo_scanner_3.png)


* Como conclusión final hemos observado que el depurador de *IntelliJ Idea* trae por defecto unas configuraciones muy parecidas al de *Eclipse*, pero *IntelliJ Idea* además de que tiene más tipos de puntos de ruptura (no nos hemos metido en profundidad porque es algo que no hemos dado) tambíen tiene mayor versatilidad que *Eclipse* ya que tiene una gran cantidad de herramientas y configuraciones para cambiar el funcionamiento de nuestro depurador de *IntelliJ Idea* según cómo sea nuestro programa y el objetivo de la depuración del mismo.
